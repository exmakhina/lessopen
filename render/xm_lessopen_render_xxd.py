#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import render_with_call

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if 0 \
	 or mimetype in ("application/octet-stream",) \
	 or 0:
		cmd = "xxd -u -g 1".split() + [path]
		return render_with_call(path, mimetype, cmd)
