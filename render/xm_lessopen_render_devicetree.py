#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import render_with_call


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype.startswith("application/x-dtb"):
		logger.info("Showing device tree blob")
		cmd = ["dtc", "--in-format=dtb", path]
		return render_with_call(path, mimetype, cmd)
