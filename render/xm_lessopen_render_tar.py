#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, subprocess, logging


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype in (
	 "application/x-compressed-tar",
	):
		cmd = ['tar', '--list', '--file', path]
		subprocess.call(cmd)
		return True
