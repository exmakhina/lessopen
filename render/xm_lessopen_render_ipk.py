#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import check_exe

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype == "application/vnd.debian.binary-package":
		import libarchive
		import libarchive.entry
		import libarchive.ffi

		with libarchive.file_reader(path, "ar") as archive:
			for entry in archive:
				print(entry)

				data = b"".join([block for block in entry.get_blocks()])

				if entry.name == "control.tar.gz":
					print("control:")
					with libarchive.memory_reader(data, "tar", "gzip") as archive:
						for entry in archive:
							data = b"".join([block for block in entry.get_blocks()])
							print("- {}:".format(entry.name))
							print(data.decode("utf-8"))

				elif entry.name == "data.tar.xz":
					print("data:")
					with libarchive.memory_reader(data, "tar", "xz") as archive:
						for entry in archive:
							print("- {} {}".format(entry.size, entry.name))

				elif entry.name == "data.tar.zst":
					print("data:")
					with libarchive.memory_reader(data, "tar", "zstd") as archive:
						for entry in archive:
							print("- {} {}".format(entry.size, entry.name))

		return True
