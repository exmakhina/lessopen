#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import check_exe

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if path.endswith((".nii", ".nii.gz")):
		cmd = [
		 os.path.expanduser("~/Work/exmakhina/neuropoly/code/spinalcordtoolbox/scripts/sct_nifti_tool.py"),
		 'textconv',
		 filename,
		]
		logger.info("Preview NIFTI file")
		out, err = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()
		out = out.decode(sys.stdout.encoding or 'utf-8')
		printf(out)
		return True
