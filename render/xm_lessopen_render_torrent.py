#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, subprocess, logging


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype in (
	 "application/x-bittorrent",
	):
		logger.info("Showing Bittorrent file %s", filename)
		cmd = [
		 "torrent.py", "info", filename,
		]
		subprocess.call(cmd)
		return True
