#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

# View pem / der

import sys, io, os, re, subprocess, logging


from ..functions import render_with_call

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	logger.info("Mimetype: %s", mimetype)
	if 0 \
	 or mimetype in ("application/x-x509-ca-cert", "application/pkix-cert", "application/pkix-cert+pem") \
	 or 0:
		cmd = [
		 "openssl",
		  "x509",
		   "-in", path,
		   "-text",
		   "-noout",
		]
		render_with_call(path, mimetype, cmd)
		return True

	if 0 \
	 or mimetype in ("application/x-pem-key",) \
	 or 0:
		cmd = [
		 "openssl",
		  "rsa",
		   "-noout",
		   "-text",
		   "-in", path,
		]
		render_with_call(path, mimetype, cmd)
		return True

	if 0 \
	 or mimetype in ("application/x-pem-pubkey", "application/x-pem-key") \
	 or 0:
		cmd = [
		 "openssl",
		  "asn1parse",
		   "-inform", "pem",
		   "-in", path,
		]
		render_with_call(path, mimetype, cmd)
		cmd = [
		 "cat",
		 path,
		]
		render_with_call(path, mimetype, cmd)
		return True

	# EC/public
	# openssl ec -in public.pem -pubin -text -noout
	# EC/private
	# openssl ec -in public.pem -text -noout
	
