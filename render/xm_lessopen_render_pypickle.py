#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import check_exe

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype == "application/x-python-pickle":
		logger.info("Showing Python Pickle %s" % path)
		import pickletools
		pickletools.dis(buf)
		return True

