#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import check_exe

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype == "text/html":
		logger.info("Preview HTML file with elinks")
		
		elinks = check_exe("elinks")

		if not elinks:
			return False
	
		cmd = 'elinks -dump 1 -dump-color-mode 1'.split() + [path]
		subprocess.call(cmd)
		return True
