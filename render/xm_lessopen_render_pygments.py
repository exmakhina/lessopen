#!/usr/bin/env python
# -*- coding: utf-8
# Render with pigments

import sys, io, os, logging

import pygments
import pygments.lexers
import pygments.formatters
import pygments.util

logger = logging.getLogger(__name__)


# pygments/lexers/_mapping.py

xdg_mimetype_to_pygments_mimetype = {
 "application/x-yaml": "text/x-yaml",
 "application/x-perl": "text/x-perl",
}


def render_from_buffer(mimetype, buf):

	mimetype = xdg_mimetype_to_pygments_mimetype.get(mimetype, mimetype)

	lexer_options = {
	 'encoding': 'utf-8',
	}

	probed = False

	if not probed:
		try:
			import libchardet
			d = libchardet.Detector()
			res = d.feed(buf)
			for i, (encoding, proba) in enumerate(res):
				if i > 10: break
				#print("%5.3f: %s" % (proba, encoding))
			encoding, proba = res[0]
			if proba >= 0.5:
				lexer_options['encoding'] = encoding
				probed = True
		except ImportError:
			pass

	if not probed:
		try:
			import cchardet as chardet
			result = chardet.detect(buf)
			encoding = result['encoding'] or 'ascii'
			if result['confidence'] > 0.5:
				lexer_options['encoding'] = encoding
				probed = True
		except ImportError:
			pass

	if not probed:
		try:
			#from chardet import constants
			#constants._debug = True
			from chardet.universaldetector import UniversalDetector
			u = UniversalDetector()
			for i, line in enumerate(buf.splitlines()):
				u.feed(line)
			u.close()
			result = u.result
			encoding = result['encoding'] or 'iso8859-15'
			#print(result)
			lexer_options['encoding'] = encoding
		except ImportError:
			pass

	try:
		lexer = pygments.lexers.get_lexer_for_mimetype(mimetype,
		 **lexer_options)
		#print(lexer)
	except pygments.util.ClassNotFound:
		logger.debug("Cannot handle %s", mimetype)
		return False

	sys.stdout.write(f"\x1B[38;1mRendering {mimetype} with pygments\x1B[0m\n")
	sys.stdout.flush()

	TERM = os.environ.get("TERM")

	formatter_options = {
	 'style': 'monokai',
	 'encoding': 'utf-8',
	}
	formatter_name = 'terminal256' if "256" in TERM else 'terminal'
	formatter = pygments.formatters.get_formatter_by_name(
	 formatter_name,
	 **formatter_options
	)

	class Str():
		def __init__(self):
			self.bufs = []
		def __del__(self):
			lines = "".join(self.bufs).splitlines(True)
			l = len(str(len(lines)))
			fmt = "%" + str(l) + "d" + ":"
			for i, line in enumerate(lines):
				sys.stdout.write(fmt % (i+1))
				sys.stdout.write(line)
			sys.stdout.flush()
		def write(self, x):
			self.bufs.append(x)

	if os.environ.get('LESSPIPE_LINUM', None) is not None:
		stream = Str()
	else:
		stream = sys.stdout.buffer


	pygments.highlight(buf, lexer, formatter, stream)
	return True


def can_render(mimetype):
	if mimetype.startswith("text"):
		return True
	if mimetype in {
	 "application/x-yaml",
	 "application/x-perl",
	}:
		return True

def render_from_path(mimetype, x):
	if not can_render(mimetype):
		return
	with io.open(x, "rb") as fi:
		buf = fi.read()
	return render_from_buffer(mimetype, buf)


def render_from_stream(mimetype, fi):
	if not can_render(mimetype):
		return
	buf = fi.read()
	return render_from_buffer(mimetype, buf)
