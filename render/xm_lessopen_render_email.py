#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-lesspipe@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging
import sys
import io
import os
import tempfile
import subprocess
import base64
import email
import mailbox


from ..xm_lessopen import render

logger = logging.getLogger(__name__)


def decode_header(x):
		v = email.header.decode_header(x)
		lns = []
		for m in v:
				c, e = m
				if isinstance(c, str) and e is None:
						lns.append(c)
				elif isinstance(c, bytes) and e is None:
						lns.append(c.decode('utf-8'))
				elif isinstance(c, bytes):
						try:
								d = c.decode(e)
						except:
								w = '%s/%s'  % (c.decode('iso8859-15'), e)
								try:
										d = c.decode('utf-8')
										logger.info("don't know how to decode %s, fell back to utf-8 with %s", w, d)
								except:
										d = c.decode('iso8859-15')
										logger.info("don't know how to decode %s, fell back to iso8859-15 with %s", w, d)
						lns.append(d)
				else:
						lns.append(str(m))
						logger.warning("don't know how to decode %s", m)
		return "".join(lns)

def unpack_mail(mail):
	print("\x1B[32mlessopen begin showing e-mail\x1B[0m")
	for k, v in mail.items():
		v = decode_header(v)
		sys.stdout.write(f"\x1B[32m{k}\x1B[0m: {v}\n")
		sys.stdout.flush()
	if mail.is_multipart():
		for part in mail.get_payload():
			unpack_mail(part)
	else:
		with tempfile.NamedTemporaryFile(
		 #prefix="temp_",
		 #dir=os.getcwd(),
		 #delete=True,
		 ) as temp_file:
			if mail.get("Content-Transfer-Encoding") == "base64":
				data = base64.b64decode(mail.get_payload())
			else:
				data = mail.get_payload().encode()

			temp_file.write(data)
			temp_file.flush()
			print("Beg rendering part")
			render(temp_file.name)
			print("\x1B[0mEnd rendering part")
	print("\x1B[32mlessopen end showing e-mail\x1B[0m")


def render_from_path(mimetype, path):
	if mimetype in ("application/mbox",):
		mbox = mailbox.mbox(path)
		for message in mbox:
			unpack_mail(message)
		return True

	if mimetype in ("message/rfc822","SMTP mail, ASCII text"):
		with io.open(path, "rb") as fi:
			data = fi.read()

		try:
			mail = email.message_from_bytes(data)
			unpack_mail(mail)
		except UnicodeDecodeError:
			sys.stdout.write(str(data))
			raise

		return True

