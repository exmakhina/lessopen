#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import render_with_call


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype.startswith("application/x-sharedlib"):
		cmd = "objdump --all-headers --dynamic-syms".split() + [path]
		return render_with_call(path, mimetype, cmd)
	if mimetype.startswith("application/x-executable"):
		cmd = "objdump --all-headers --dynamic-syms".split() + [path]
		cmd = "readelf --all".split() + [path]
		return render_with_call(path, mimetype, cmd)
	if mimetype.startswith("application/x-object"):
		#cmd = "objdump --all-headers".split() + [path]
		#subprocess.call(cmd)
		#render_with_call(path, mimetype, cmd)
		cmd = [
		 "objdump",
		 #"--dynamic-syms",
		 "--disassemble-all",
		 path,
		]
		return render_with_call(path, mimetype, cmd)
