#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, subprocess, tempfile, logging


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype in ("application/pdf",):
		logger.info("Rendering with %s", __file__)
		tmpf = tempfile.mktemp(prefix="lesspipe-", suffix=".txt")
		cmd = ['pdftotext', path, tmpf]
		subprocess.call(cmd)
		with open(tmpf, "rb") as f:
			os.write(1, f.read())
		os.unlink(tmpf)
		return True

