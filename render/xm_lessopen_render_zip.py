#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, subprocess, logging

from ..functions import check_exe


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):

	prog = check_exe("7z")
	if not prog:
		return

	if mimetype in (
	 "application/zip",
	):
		logger.info("lesspipe: showing archive with 7z")
		cmd = [prog, "l", path]
		subprocess.call(cmd)
		return True
