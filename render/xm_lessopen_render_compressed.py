#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, logging

from ..functions import chainrender_with_call


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype == 'application/x-bzip':
		logger.info("Rendering compressed file %s", mimetype)
		cmd = ['bzip2', '--decompress', '--stdout', path]
		return chainrender_with_call(path, mimetype, cmd)

	if mimetype == 'application/x-xz':
		logger.info("Rendering compressed file %s", mimetype)
		cmd = ['xz', '--decompress', '--stdout', path]
		return chainrender_with_call(path, mimetype, cmd)
