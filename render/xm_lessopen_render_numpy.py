#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-lessopen@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype == "application/x-numpy":
		try:
			import numpy as np
		except ImportError:
			return False
		with open(path, "rb") as fi:
			major, minor = np.lib.format.read_magic(fi)
			logger.info("Showing Python Numpy %d.%d", major, minor)
			if major == 1:
				shape, fortran_order, dtype \
				 = np.lib.format.read_array_header_1_0(fi)
			if major == 2:
				shape, fortran_order, dtype \
				 = np.lib.format.read_array_header_2_0(fi)
			header_len = fi.tell()
			try:
				arr = np.load(path, mmap_mode="r", allow_pickle=False)
			except Exception as e:
				logger.info("Error: %s", e)
				return False

			print("\x1B[32mNumpy array %d.%d hdrlen=%d"
			 " shape=%s dtype=%s fortran=%d\x1B[0m"
			 % (major, minor, header_len, shape, dtype, fortran_order))
			print(arr)
		return True
