#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, subprocess, tempfile, shutil, logging


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if 0 \
	 or mimetype.startswith("application/vnd.oasis.opendocument.") \
	 or mimetype in ("application/msword",) \
	 or mimetype.startswith("application/vnd.openxmlformats-officedocument.") \
	 or 0:
		tmpdir = tempfile.mktemp(prefix="lesspipe-")
		os.mkdir(tmpdir)

		if "spreadsheet" in mimetype:
			# multi-sheet
			fmt = "pdf"

			cmd = ['libreoffice',
			 '--headless',
			 '--convert-to', fmt,
			 '--outdir', tmpdir,
			 path,
			]
			subprocess.call(cmd, stdout=subprocess.PIPE)

			for cwd, dirs, files in os.walk(tmpdir):
				for file in files:
					path = os.path.join(cwd, file)
					cmd = [ "pdftotext", path, "-" ]
					subprocess.call(cmd)

			shutil.rmtree(tmpdir)
			return True

		else:
			fmt = "txt:Text"

			cmd = ['libreoffice',
			 '--headless',
			 '--convert-to', fmt,
			 '--outdir', tmpdir,
			 path,
			]
			subprocess.call(cmd, stdout=subprocess.PIPE)

			for cwd, dirs, files in os.walk(tmpdir):
				for file in files:
					path = os.path.join(cwd, file)
					with open(path, 'rb') as f:
						os.write(1, f.read())

			shutil.rmtree(tmpdir)
			return True

