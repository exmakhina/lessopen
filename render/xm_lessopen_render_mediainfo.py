#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT


import sys, io, os, re, subprocess, tempfile, subprocess, logging

from ..functions import check_exe

logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if 0 \
	 or mimetype.startswith("image/") \
	 or mimetype.startswith("video/") \
	 or mimetype.startswith("audio/") \
	 or 0:
		cmd = ['mediainfo', path]
		proc = subprocess.Popen(cmd,
		 stdout=subprocess.PIPE,
		)
		out, err = proc.communicate()
		if sys.stdout.encoding:
			out = out.decode(sys.stdout.encoding)
		else:
			out = out.decode()
		for x in out.split('\n'):
			if not x:
				continue
			m = re.match('(?P<key>.*?):(?P<value>.*)', x)
			if m is not None:
				key, value = m.groups()
				print("\x1B[33m%s\x1B[0m:\x1B[32m%s\x1B[0m" % (key, value))
			else:
				print("\x1B[31m%s\x1B[0m" % x)
		if 'image' in mimetype and 0:
			cmd = ['img2txt', filename]
			subprocess.call(cmd)
		if 'image' in mimetype:
			term = os.environ.get("TERM")
			if term in ("xterm-256color"):
				args = []
			elif term.startswith("screen"):
				args = ["--penetrate"]
			else:
				args = []

			logger.debug("img2sixel args: %s", args)
			sys.stdout.flush()
			img2sixel = check_exe("img2sixel")
			# could also use "convert"
			if img2sixel is not None:
				cmd = [img2sixel] + args + [path]
				subprocess.call(cmd)

			# https://pypi.org/project/libsixel-python/0.4.0/

		return True
