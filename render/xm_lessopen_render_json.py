#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import sys, io, os, subprocess, logging

from ..functions import render_with_call
from ..functions import render_stream_with_call


logger = logging.getLogger(__name__)


def render_from_path(mimetype, path):
	if mimetype in (
	 "application/json",
	):
		cmd = [
		 "jq",
		 "--color-output",
		 ".",
		 path,
		]
		return render_with_call(path, mimetype, cmd)


def render_from_stream(mimetype, fi):
	if mimetype in (
	 "application/json",
	):
		cmd = [
		 "jq",
		 "--color-output",
		]
		return render_stream_with_call(fi, mimetype, cmd)
