#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# less pipe

import sys, os, logging

from .xm_lessopen import *

logger = logging.getLogger(__name__)

def main(args=None):
	import argparse

	parser = argparse.ArgumentParser(
	 description="Textual representer",
	)

	default_log_level = os.environ.get("LESSPIPE_LOG_LEVEL", "WARNING")

	parser.add_argument("--log-level",
	 default=default_log_level,
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("path",
	 nargs="?",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args(args)

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(pathname)s:%(lineno)s %(name)s %(levelname)s %(message)s"
	)

	if "DISPLAY" in os.environ:
		del os.environ['DISPLAY']

	if args.path is None:
		f = sys.stdin.buffer
	else:
		f = args.path

	try:
		render(f)
	except BrokenPipeError:
		pass

if __name__ == '__main__':
	main()
