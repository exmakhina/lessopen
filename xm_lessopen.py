#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

# less pipe

from typing import *
import sys, io, os, re, subprocess, shutil, tempfile, logging
import types

logger = logging.getLogger(__name__)


def plugin_files(prefix) -> List[str]:
	plugins = list()
	dirs = [ os.path.dirname(__file__) ]
	for dir_ in dirs:
		for cwd, ds, fs in os.walk(dir_):
			ds.sort()
			fs.sort()
			for f in fs:
				if not f.endswith(".py"):
					continue
				if not f.startswith(prefix):
					continue
				p = os.path.join(cwd, f)
				plugins.append(p)
	return sorted(plugins)


def load_plugin(path):
	dirname, basename = os.path.split(path)
	stem, ext = os.path.splitext(basename)

	try:
		with io.open(path, "rb") as f:
			code_data = f.read()
	except Exception as e:
		logger.warning("Couldn't load plugin %s: %s",
		 path, e, exc_info=True)
		return

	try:
		code = compile(code_data, stem, "exec")
	except Exception as e:
		logger.warning("Couldn't compile plugin %s: %s",
		 path, e, exc_info=True)
		return

	if path.startswith(os.path.dirname(__file__)):
		modname = "{}{}".format(__name__.rsplit(".", 1)[0], path[len(os.path.dirname(__file__)):].replace("/", ".").replace(".py", ""))
	else:
		modname = stem

	logger.info("Module name: %s", modname)

	mod = types.ModuleType(modname)
	mod.__file__ = path
	try:
		exec(code, mod.__dict__)
	except ModuleNotFoundError as e:
		logger.warning("Couldn't exec plugin %s: missing dep %s", path, e, exc_info=True)
		return
	except ImportError as e:
		logger.warning("Couldn't exec plugin %s: %s", path, e, exc_info=True)
		return
	except Exception as e:
		logger.warning("Couldn't exec plugin %s: %s", path, e, exc_info=True)
		return

	return mod


def load_plugins(prefix):
	paths = plugin_files(prefix)
	plugins = list()

	for path in paths:
		plugin = load_plugin(path)
		if plugin is None:
			continue
		plugins.append(plugin)

	return plugins



def find_mime_from_data(data):
	plugins = load_plugins("xm_lessopen_discover_")
	logger.info("Plugins(find): %s", plugins)

	t = None
	v = -1

	for plugin in plugins:
		logger.debug("- %s", plugin.__name__)

		func = getattr(plugin, "find_mime_from_data", None)
		if not func:
			continue

		try:
			mimetype = func(data)
			if mimetype:
				mimetype, score = mimetype
				logger.debug(" %s got %s with %f", plugin.__name__, mimetype, score)
				if mimetype is not None and score > v:
					v = score
					t = mimetype
		except Exception as e:
			logger.warning("Failed to find mime with %s: %s",
			 plugin, e, exc_info=True)

	return t


def find_mime_from_path(path):
	if not os.path.exists(path):
		raise FileNotFoundError(path)

	plugins = load_plugins("xm_lessopen_discover_")
	logger.info("Plugins(find): %d", len(plugins))

	t = None
	v = -1

	for plugin in plugins:
		logger.debug("- %s", plugin.__name__)

		func = getattr(plugin, "find_mime_from_path", None)
		if not func:
			continue

		try:
			mimetype = func(path)
			if mimetype:
				mimetype, score = mimetype
				logger.debug(" %s got %s with %f", plugin.__name__, mimetype, score)
				if mimetype is not None and score > v:
					v = score
					t = mimetype

		except Exception as e:
			logger.warning("Failed to find mime with %s: %s",
			 plugin, e, exc_info=True)

	return t


def render_from_path(mimetype, path):
	if not os.path.exists(path):
		raise FileNotFoundError(path)
	plugins = load_plugins("xm_lessopen_render_")
	logger.info("Plugins(render): %d", len(plugins))

	for plugin in plugins:
		logger.debug("- %s", plugin.__name__)

		func = getattr(plugin, "render_from_path", None)
		if not func:
			continue

		try:
			if func(mimetype, path):
				break
		except BrokenPipeError:
			pass
		except Exception as e:
			logger.warning("Failed to render with %s: %s",
			 plugin, e, exc_info=True)
	else:
		with io.open(path, "rb") as fi:
			while True:
				data = fi.read(4096)
				if not data:
					break
				sys.stdout.buffer.write(data)
				sys.stdout.buffer.flush()


def render_from_stream(mimetype, fi):
	plugins = load_plugins("xm_lessopen_render_")
	logger.info("Plugins(render): %d", len(plugins))

	for plugin in plugins:
		logger.debug("- %s", plugin.__name__)

		func = getattr(plugin, "render_from_stream", None)
		if not func:
			continue

		try:
			if func(mimetype, fi):
				break
		except BrokenPipeError:
			pass
		except Exception as e:
			logger.warning("Failed to render with %s: %s",
			 plugin, e, exc_info=True)
	else:
		while True:
			data = fi.read(4096)
			if not data:
				break
			sys.stdout.buffer.write(data)
			sys.stdout.buffer.flush()


def render_from_buffer(mimetype, fi):
	plugins = load_plugins("xm_lessopen_render_")
	logger.info("Plugins(render): %d", len(plugins))


	for plugin in plugins:
		logger.info("- %s", plugin.__name__)

		func = getattr(plugin, "render_from_buffer", None)
		if not func:
			continue

		try:
			if func(mimetype, fi):
				break
		except BrokenPipeError:
			pass
		except Exception as e:
			logger.warning("Failed to render with %s: %s",
			 plugin, e, exc_info=True)
	else:
		buf = fi
		sys.stdout.buffer.write(buf)
		sys.stdout.buffer.flush()


class PeekableStream:
	def __init__(self, fi):
		self._fi = fi
		self._bi = io.BytesIO()
		self._bp = 0

	def peek(self, l):
		res = self._fi.read(l)
		self._bi.seek(0, 2)
		self._bi.write(res)
		return res

	def read(self, l=None):
		self._bi.seek(0, 2)
		bs = self._bi.tell() - self._bp
		if bs == 0:
			res = self._fi.read(l)
			return res
		elif l is not None and bs >= l:
			self._bi.seek(self._bp, 0)
			res = self._bi.read(l)
			self._bp += len(res)
			return res
		else:
			self._bi.seek(self._bp, 0)
			res = self._bi.read(l)
			self._bp += len(res)
			if l is not None:
				l -= len(res)
			res += self._fi.read(l)
			return res


def render(f):
	try:
		if isinstance(f, str):
			if not os.path.exists(f):
				raise FileNotFoundError(f)
			mimetype = find_mime_from_path(f)
			logger.info("Filetype=%s", mimetype)
			sys.stderr.flush()
			render_from_path(mimetype, f)
			sys.stdout.buffer.flush()
		else:
			s = PeekableStream(f)
			x = s.peek(2<<20)
			mimetype = find_mime_from_data(x)
			logger.info("Filetype=%s", mimetype)
			render_from_stream(mimetype, s)
			sys.stdout.buffer.flush()
	except BrokenPipeError:
		pass

