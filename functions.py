#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import sys, os, logging, subprocess

if sys.hexversion < 0x03030000:
	import pipes
	def list2cmdline(lst):
		return " ".join(pipes.quote(x) for x in lst)
else:
	import shlex
	def list2cmdline(lst):
		return " ".join(shlex.quote(x) for x in lst)


logger = logging.getLogger(__name__)


def check_exe(name, env=None):
	"""
	Ensure that a program exists
	:type name: string
	:param name: name or path to program
	:return: path of the program or None
	"""
	if env is None:
		env = os.environ

	if not name:
		raise ValueError('Cannot execute an empty string!')
	def is_exe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(name)
	if fpath and is_exe(name):
		return os.path.abspath(name)
	else:
		for path in env["PATH"].split(os.pathsep):
			path = path.strip('"')
			exe_file = os.path.abspath(os.path.join(path, name))
			if is_exe(exe_file):
				return exe_file
	return None


def render_with_call(path, mimetype, cmd, **kw):
	prog = cmd[0]
	if check_exe(prog) is None:
		return

	logger.info("Calling %s", list2cmdline(cmd))
	sys.stdout.write("\x1B[38;1mRendering {} with “{}”\x1B[0m\n".format(
	 mimetype, list2cmdline(cmd)))
	sys.stdout.flush()
	subprocess.call(cmd, **kw)
	return True


def chainrender_with_call(path, mimetype, cmd, **kw):
	prog = cmd[0]
	if check_exe(prog) is None:
		return

	logger.info("Calling %s", list2cmdline(cmd))
	sys.stdout.write("\x1B[38;1mChain-rendering {} with “{}”\x1B[0m\n".format(
	 mimetype, list2cmdline(cmd)))
	sys.stdout.flush()

	cmd = "{} | {}".format(list2cmdline(cmd), sys.argv[0])

	subprocess.call(cmd, shell=True)
	return True


def render_stream_with_call(fi, mimetype, cmd, **kw):
	prog = cmd[0]
	if check_exe(prog) is None:
		return

	logger.info("Calling %s", list2cmdline(cmd))
	sys.stdout.write("\x1B[38;1mRendering {} with “{}”\x1B[0m\n".format(
	 mimetype, list2cmdline(cmd)))
	sys.stdout.flush()
	proc = subprocess.Popen(cmd,
	 stdin=subprocess.PIPE,
	 **kw)

	l = 0
	while True:
		data = fi.read(4096)
		l += len(data)
		if not data:
			break
		proc.stdin.write(data)
		logger.info("pouet")
	logger.info("close after %d", l)
	proc.stdin.close()
	proc.wait()

	return True
