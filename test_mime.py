#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import os
import logging

from .xm_lessopen import find_mime_from_path


logger = logging.getLogger(__name__)


def test_mime():

	root = "samples"
	for cwd, ds, fs in os.walk(root):

		ds.sort()
		fs.sort()
		for f in fs:
			path = os.path.join(cwd, f)
			mime = find_mime_from_path(path)
			logger.info("mime: %s", mime)

			reldir = os.path.relpath(os.path.dirname(path), root)

			if reldir != ".":
				assert mime == reldir

def test_one():
	path = "16VnMcF1KJYxN9QId6TClMsZRahHNMW5g"
	mime = find_mime_from_path(path)
	logger.info("mime: %s", mime)
