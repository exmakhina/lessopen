.. -*- coding: utf-8; indent-tabs-mode:nil; -*-
.. SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
.. SPDX-License-Identifier: MIT

#####################
Yet another less pipe
#####################

This is an input pre-processor for some file types.

License: MIT (see SPDX headers & LICENSES/).


Background
##########

``less`` can use an input preprocessor (see less manual, § INPUT
PREPROCESSOR); use of the preprocessor is via specification of a
`LESSOPEN` environment variable.


Design
######

An input preprocessor can be used for other purposes than less'ing
files, the goal is to get a textual representation of data.

This preprocessor is based on discovering the MIME type of input data,
with discovery plug-ins, and rendering with rendering plug-ins.
This allows it to work not only on paths, but also on streams or
buffers of data.


Usage
#####


With less
*********

Add this lesspipe to your PATH, and add this:

.. code:: sh

   export LESSOPEN="| lesspipe %s"


With git
********

This can be helpful as a git diff textconv (see `gitattributes(5)`
§ Performing text diffs of binary files), with, eg:


- config::

    [diff "xm_lessopen"]
      textconv = ~/.local/opt/xm_lessopen/lesspipe


- attributes::

    *.ods diff=xm_lessopen
    *.fods diff=xm_lessopen
    *.pdf diff=xm_lessopen
    *.docx diff=xm_lessopen
    ...


Design
######

The tool first sniffs the media type of the input data using various
discovery plugins, then it tries to use representation plug-ins to
render the data.


References
**********

Existing relevant stuff that is not used here:
- mc pretty-print, using ext.d viewers / openers

  in::

    /usr/libexec/mc/ext.d/


- src-hilite-lesspipe.sh %s

- https://github.com/wofr06/lesspipe

