#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# JSON finder

import logging


logger = logging.getLogger(__name__)


def find_mime_from_path(path):
	if not path.endswith(".rst"):
		return

	try:
		import docutils.parsers.rst
		parser = docutils.parsers.rst.Parser()
		source = path
		settings = None
		document = docutils.utils.new_document(source, settings)
		document.settings.pep_references = False
		document.settings.rfc_references = False

		with open(path, "r", encoding="utf-8") as fi:
			input = fi.read()
			parser.parse(input, document)
		return "text/x-rst", 1.0
	except Exception as e:
		pass


def find_mime_from_data(buf):
	try:
		input = buf.decode("utf-8")
		import docutils.parsers.rst
		parser = docutils.parsers.rst.Parser()
		source = "/dev/stdin"
		settings = None
		document = docutils.utils.new_document(source, settings)
		document.settings.pep_references = False
		document.settings.rfc_references = False
		parser.parse(input, document)
		return "text/x-rst", 1.0
	except Exception as e:
		raise
		pass

