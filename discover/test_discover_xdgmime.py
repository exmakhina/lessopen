# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

import io

from xm_lessopen_discover_xdgmime import *


logger = logging.getLogger(__name__)


def test_load_discover():
	try:
		data = b"\xfd\xae\x65\x56\x01\x57"
		with io.open("/home/cJ/Work/exmakhina/meta/accounting/invoices-voti-2021.fods", "rb") as fi:
			data = fi.read()
		mimetype = find_mime_from_data(data)
		logger.info("Mime: %s/“%s”", type(mimetype).__name__, mimetype)
	except Exception as e:
		logger.warning("Failed to find mime: %s", e, exc_info=True)

def test_path():
	path = "/home/cJ/Work/exmakhina/meta/accounting/invoices-voti-2021.fods"
	mimetype = find_mime_from_path(path)
	logger.info("Mime: %s/“%s”", type(mimetype).__name__, mimetype)
