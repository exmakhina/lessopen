#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

# less pipe

import logging
import ctypes

logger = logging.getLogger(__name__)

try:
	libxdgmime = ctypes.CDLL("libxdgmime.so")
except OSError:
	raise ImportError

get_mime_type_for_data = libxdgmime.xdg_mime_get_mime_type_for_data

get_mime_type_for_data.restype = ctypes.c_char_p
get_mime_type_for_data.argtypes = (
 ctypes.c_char_p, ctypes.c_size_t, ctypes.POINTER(ctypes.c_int))

get_mime_type_for_file = libxdgmime.xdg_mime_get_mime_type_for_file
get_mime_type_for_file.restype = ctypes.c_char_p
get_mime_type_for_file.argtypes = (
 ctypes.c_char_p, ctypes.c_void_p)


def find_mime_from_path(path):
	"""
	"""
	with open(path, "rb") as fi:
		data = fi.read(2048)
		return find_mime_from_data(data)

	i = ctypes.c_int(1)
	filemime = get_mime_type_for_file(path.encode("utf-8"), ctypes.byref(i)) #ctypes.c_void_p(0)
	filetype = filemime.decode()

	return filetype, i.value / 100


def find_mime_from_data(data):
	"""
	"""
	i = ctypes.c_int(1)
	filemime = get_mime_type_for_data(data, len(data), ctypes.byref(i))
	filemime = filemime.decode()

	if filemime is None:
		return

	filetype = str(filemime)
	return filetype, i.value / 100
