# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

from xm_lessopen import *


logger = logging.getLogger(__name__)


def test_load_discover():
	plugins = load_plugins("xm_lessopen_discover_")
	logger.info("Plugins: %s", plugins)

	for plugin in plugins:
		logger.info("- %s: %s", plugin, dir(plugin))

		try:
			mimetype = plugin.find_mime_from_data(b"\xfd\xae\x65\x56\x01\x57")
			logger.info("Mime: %s/“%s”", type(mimetype).__name__, mimetype)
		except Exception as e:
			logger.warning("Failed to find mime: %s", e, exc_info=True)
