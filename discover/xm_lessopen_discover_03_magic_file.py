

def find_mime_from_path(path):
	import magic
	try:
		filetype = magic.from_file(path, mime=True)
	except AttributeError:
		mgc = magic.open(0)
		mgc.load()
		filetype = mgc.file(path)
		mgc.close()
	if filetype in ("data", "empty"):
		return None
	return filetype, 0.4


def find_mime_from_data(data):
	import magic
	filetype = magic.from_buffer(data, mime=True)
	if filetype in ("data", "empty"):
		return None
	return filetype, 0.4
