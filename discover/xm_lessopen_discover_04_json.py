#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT
# JSON finder

import logging
import json

logger = logging.getLogger(__name__)

def find_mime_from_path(path):
	if not path.endswith(".json"):
		return

	try:
		with open(path, "r", encoding="utf-8") as fi:
			json.load(fi)
		return "application/json", 1.0
	except:
		pass


def find_mime_from_data(data):
	try:
		v = data.decode("utf-8")
		json.loads(v)
		return "application/json", 1.0
	except:
		pass
