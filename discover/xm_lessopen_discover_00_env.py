import os

def find_mime_from_path(path):
	env = os.environ.get("LESS_MIME", None)
	if env is not None:
		return env, 1.0


def find_mime_from_data(data):
	env = os.environ.get("LESS_MIME", None)
	if env is not None:
		return env, 1.0

