#!/usr/bin/env python
# -*- coding: utf-8; vi: noet
# SPDX-FileCopyrightText: 2013-2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: MIT

# less pipe

import logging

import xdg.Mime

logger = logging.getLogger(__name__)

xdg.Mime.update_cache()
logger.info("xdg.Mime has %d types", len(xdg.Mime.types))


def find_mime_from_path(path):
	"""
	"""
	filemime = xdg.Mime.get_type_by_contents(path)

	if filemime is None:
		return

	filetype = str(filemime)

	# TODO remove
	if filetype == "application/octet-stream":
		if path.endswith(".pkl"):
			return "application/x-python-pickle", filemime

	return filetype, 0.5


def find_mime_from_data(data):
	"""
	"""
	filemime = xdg.Mime.get_type_by_data(data)
	if filemime is None:
		return

	filetype = str(filemime)
	return filetype, 0.5

